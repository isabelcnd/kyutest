import { Component, OnInit } from '@angular/core';
import { ApiRequestService } from '../../services/api-request.service';
import { EventsService } from 'src/app/services/events/events.service';
import { CdkDragDrop, moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { reject, resolve } from 'q';


@Component({
  selector: 'app-view-images',
  templateUrl: './view-images.component.html',
  styleUrls: ['./view-images.component.css']
})
export class ViewImagesComponent implements OnInit {
  private listImages:Array<any> = [];
  private /* @Input()  */imgSrc: string;
  private loading:boolean;

  constructor(private api: ApiRequestService, private events: EventsService, private modalService: NgbModal) { }

  ngOnInit() {
    this.loading = true;
    console.log("loading");
    let promise = new Promise((resolve, reject)=> {
      this.api.getImages().then((data:Array<any>)=> {
        this.listImages = data;
        this.listImages.sort((item1, item2) => {
          if ((item1.title.length - item2.title.length) === 0) {
            return parseInt(item2.id) - parseInt(item1.id);
          }
          else {
            return item1.title.length - item2.title.length;
          }
        });
        resolve(this.listImages);
      },(error) => {
        reject(error);
      });
    });
    promise.then(()=> {
      this.loading = false;
      console.log("end loading");
    }).catch((error) => {
      console.log("error: ", error);
    })
  }

  open(item:any, content:any) {
    this.imgSrc = item;
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      // this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      // this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

  moveElement(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.listImages, event.previousIndex, event.currentIndex);
  }

}
