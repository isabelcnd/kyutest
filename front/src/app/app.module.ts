import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

// styles
import { ModalModule } from 'ngx-bootstrap/modal';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

// drag and drop
import { DragDropModule } from '@angular/cdk/drag-drop';

// components
import { AppComponent } from './app.component';
import { ViewImagesComponent } from './components/view-images/view-images.component';

// services
import { ApiRequestService } from './services/api-request.service';
import { HttpClientModule } from '@angular/common/http';
import { EventsService } from './services/events/events.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    AppComponent,
    ViewImagesComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ModalModule.forRoot(),
    BrowserAnimationsModule,
    DragDropModule,
    NgbModule
  ],
  providers: [
    ApiRequestService,
    EventsService
  ],
  bootstrap: [AppComponent],
  entryComponents: []
})
export class AppModule { }
