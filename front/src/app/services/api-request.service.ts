import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiRequestService {
  api:String = "https://jsonplaceholder.typicode.com/photos?albumId=1";

  constructor(private http: HttpClient) {
        
  }

  async getImages() {
    return await this.http.get(`${this.api}`).toPromise();
    // return this.http.get(`${this.api}`);
  }
}
