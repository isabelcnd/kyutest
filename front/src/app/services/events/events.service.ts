import { Injectable, Output, EventEmitter } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class EventsService {
  @Output() public modalAction = new EventEmitter();
  constructor() { }

  emitActionModal(action:string = "open",item:any = null) {
    // this.modalAction.next(action,item);
    let obj = {"action":action, "obj":item};
    this.modalAction.emit(obj)
  }

  getActionModal() {
    return this.modalAction;
  }
}
