<?php

namespace App\Controller;

class Vuelos
{
    private $itinerarioOriginal;
    private $itinerarioCambiado;

    function __construct() {
        $this->itinerarioOriginal = [];
        $this->itinerarioCambiado = [];
    }

    function crearVuelo($origen, $destino, $compania) {
        $vuelo = [
            "origen" => $origen,
            "destino" => $destino,
            "compania" => $compania            
        ];
        array_push($this->itinerarioOriginal, $vuelo);        
    }

    function reajustarVuelo($origen, $destino, $compania) {
        $vuelo = [
            "origen" => $origen,
            "destino" => $destino,
            "compania" => $compania            
        ];
        array_push($this->itinerarioCambiado, $vuelo);        
    }

    public function compararItinerarios() {
        $vuelosReprogramados = [];
        $vuelosMantenidos = [];
        foreach ($this->itinerarioOriginal as $key => $value) {
            $cambioBand = false;
            $cambio = $this->itinerarioCambiado;
            $vueloAgregado = -1;
            for ($i = 0; ($i < count($cambio) && !$cambioBand); $i++) {
                // detectamos si hay n cambio en el destino
                if ( ($value['origen'] == $cambio[$i]['origen']) && ($value['destino'] != $cambio[$i]['destino']) ) {
                    $busqueda = $this->searchDestino($cambio, $value['destino']);
                    array_push($vuelosReprogramados, ["original" => $value, "posicionIniRepro" => $i, "cantidadCambios" => $busqueda]);
                    $cambioBand = true;
                }
            }
            if (!$cambioBand) {
                array_push($vuelosMantenidos, $value);
            }
        }
        $auxRepro = [];
        $j = 0;
        foreach ($vuelosReprogramados as $key => $nuevoVuelo) {
            for ($i=$nuevoVuelo["posicionIniRepro"]; ($i < count($this->itinerarioCambiado) && $j < $nuevoVuelo["cantidadCambios"]); $i++) { 
                array_push($auxRepro, $this->itinerarioCambiado[$i]);
                $j++;
            }            
        }
        return ["vuelosMantenidos"=> $vuelosMantenidos, "vuelosReprogramados" => $auxRepro];
    }

    private function searchDestino($vueloRepro, $destino) {
        $cantidadIntermedia = 0;
        // $posicionRepro = -1;
        for ($i=0; $i < count($vueloRepro); $i++) { 
            if ($vueloRepro[$i]["destino"] != $destino) {
                $cantidadIntermedia++;
                // $posicionRepro = $i;
            }
        }
        return $cantidadIntermedia;
    }
}