<?php

namespace App\Controller;

class Aeroline
{
    private $listadoDeCargas;
    private $importe = 10;

    function __construct() {
        $this->listadoDeCargas = [];
    }

    function crearPaquete($origen, $destino, $cliente) {
        $paquete = [
            "origen" => $origen,
            "destino" => $destino,
            "cliente" => $cliente,
            "fecha" => new \DateTime()
        ];
        array_push($this->listadoDeCargas, $paquete);        
    }

    public function getTotalCargasPorFecha($fecha) {
        $fechaFormat = new \Datetime($fecha);
        $fechaBusqueda = explode('-',$fechaFormat->format('d-m-Y'));
        $paquetesResultantes = [];

        foreach ($this->listadoDeCargas as $key => $value) {
            $fechaExistente = explode('-',$value["fecha"]->format('d-m-Y'));
            if (($fechaExistente[0] == $fechaBusqueda[0]) && ($fechaExistente[1] == $fechaBusqueda[1]) && (($fechaExistente[2] == $fechaBusqueda[2]))) {
                array_push($paquetesResultantes, $value);
            }            
        }
        $cantidadEnviados = count($paquetesResultantes);
        return [
            "totalPaquetes" => $cantidadEnviados, 
            "totalGanancias"=> $cantidadEnviados * $this->importe,
            "paquetes" => $paquetesResultantes
        ];
    }
}