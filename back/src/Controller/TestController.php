<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Controller\Aeroline;
use App\Controller\Vuelos;

class TestController extends AbstractController
{
    private $string = "El veloz murciélago hindú comía feliz cardillo y kiwi” debe devolver murciélago.";
    private $sortedArray = [1,20,54, 33, -15, 7];
    /**
     * @Route("/test", name="test")
     */
    public function index()
    {
        $response = explode(" ",$this->string);
        $largest = strlen($response[0]);
        $position = 0;
        foreach ($response as $key => $value) {
            if (strlen($value) >= $largest) {
                $position = $key;
                $largest = strlen($value);
            }
        }
        dump($position, $response[$position]);
        return $this->json([
            'message' => '',
            'path' => 'src/Controller/TestController.php',
        ]);
    }

    /**
     * @Route("/sort", name="sortArray")
     */
    public function sortArray() {
        sort($this->sortedArray);
        dump($this->sortedArray);
        return $this->json([
            'message' => '',
            'path' => 'src/Controller/TestController.php',
        ]);
    }

    /**
     * @Route("/aero", name="aero")
     */
    public function getAeroline() {
        $aeroline = new Aeroline();
        $aeroline->crearPaquete("ori 1","sal 1", "mariano");
        $aeroline->crearPaquete("ori 2","sal 5", "mariano");
        $aeroline->crearPaquete("ori 3","sal 4", "mariano");

        $resultado = $aeroline->getTotalCargasPorFecha("2019-06-30 10:00:00");
        dump($resultado);
        return $this->json([
            'message' => '',
            'path' => 'src/Controller/TestController.php',
        ]);
    }

    /**
     * @Route("/vuelos", name="vuelos")
     */
    public function vuelos() {
        $vuelo = new Vuelos();
        $vuelo->crearVuelo("LIM","LPB", "AA");
        $vuelo->crearVuelo("LPB","UYU", "AA");
        $vuelo->reajustarVuelo("LIM","ASD", "AA");
        $vuelo->reajustarVuelo("ASD","LPB", "AA");
        $vuelo->reajustarVuelo("LPB","UYU", "AA");
        // var_dump($vuelo);

        $response = $vuelo->compararItinerarios();
        dump($response);
        return $this->json([
            'message' => '',
            'path' => 'src/Controller/TestController.php',
        ]);
    }
}
